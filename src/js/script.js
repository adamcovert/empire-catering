$(document).ready(function(){

  $(".owl-carousel").owlCarousel({
    items: 2,
    loop: true,
    center: true,
    margin: 10,
    responsive: {
      992: {
        items: 5,
      }
    }
  });

});